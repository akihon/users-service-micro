package users.service.micro;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import io.micronaut.configuration.mongo.reactive.condition.RequiresMongo;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.logging.Logger;

@RequiresMongo
@Controller("/users")
public class UsersController {

    private static final Logger log = Logger.getLogger("UsersController");

    private MongoClient mongoClient;

    public UsersController(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Get("/")
    public Single<List<User>> index() {
        return Flowable.fromPublisher(getCollection("projects", "users")
                .find()
        ).toList();
    }

    @Post("/")
    public String save(@Body User user) {
        user.set_id(ObjectId.get());
        Single
                .fromPublisher(getCollection("projects", "users")
                        .insertOne(user))
                .subscribe()
                .dispose();
        return String.format("New user has been saved with ID: %s", user.get_id());
    }

    private MongoCollection<User> getCollection(String database, String collection) {
        return this.mongoClient.getDatabase(database).getCollection(collection, User.class);
    }
}