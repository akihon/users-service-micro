package users.service.micro;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

public final class User {
    @BsonId
    private ObjectId _id;
    private String firstName;
    private String lastName;
    private String username;

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
