FROM maven:3.6.0-jdk-8-alpine as builder
COPY /src/main/resources/settings.xml /usr/share/maven/conf/settings.xml
COPY  . /root/app/
WORKDIR /root/app
RUN mvn install -DskipTests=true

FROM oracle/graalvm-ce:1.0.0-rc8 as graalvm
ENV http_proxy=
ENV https_proxy=
COPY --from=builder /root/app/ /home/app/
WORKDIR /home/app
RUN java -cp target/users-service-micro-0.1.jar \
            io.micronaut.graal.reflect.GraalClassLoadingAnalyzer \
            reflect.json
RUN native-image --no-server \
                 --class-path target/users-service-micro-0.1.jar \
    			 -H:ReflectionConfigurationFiles=/home/app/reflect.json \
    			 -H:EnableURLProtocols=http \
    			 -H:IncludeResources='logback.xml|application.yml|META-INF/services/*.*' \
    			 -H:+ReportUnsupportedElementsAtRuntime \
    			 -H:+AllowVMInspection \
    			 --rerun-class-initialization-at-runtime='sun.security.jca.JCAUtil$CachedSecureRandomHolder',javax.net.ssl.SSLContext \
    			 --delay-class-initialization-to-runtime=io.netty.handler.codec.http.HttpObjectEncoder,io.netty.handler.codec.http.websocketx.WebSocket00FrameEncoder,io.netty.handler.ssl.util.ThreadLocalInsecureRandom \
    			 -H:-UseServiceLoaderFeature \
    			 -H:Name=users-service-micro \
    			 -H:Class=users.service.micro.Application


FROM frolvlad/alpine-glibc
EXPOSE 8080
ENV MONGO_HOST=host.docker.internal
COPY --from=graalvm /home/app/users-service-micro .
ENTRYPOINT ["./users-service-micro"]