FROM oracle/graalvm-ce:1.0.0-rc8
EXPOSE 8080
ENV MONGO_HOST=host.docker.internal
COPY target/users-service-micro-*.jar users-service-micro.jar
ADD . target
RUN java -cp users-service-micro.jar io.micronaut.graal.reflect.GraalClassLoadingAnalyzer 
RUN native-image --no-server \
             --class-path users-service-micro.jar \
             -H:ReflectionConfigurationFiles=/target/reflect.json \
             -H:EnableURLProtocols=http \
             -H:IncludeResources="logback.xml|application.yml|META-INF/services/*.*" \
             -H:Name=users-service-micro \
             -H:Class=users.service.micro.Application \
             -H:+ReportUnsupportedElementsAtRuntime \
             -H:+AllowVMInspection \
             -H:-UseServiceLoaderFeature \
             --rerun-class-initialization-at-runtime='sun.security.jca.JCAUtil$CachedSecureRandomHolder,javax.net.ssl.SSLContext' \
             --delay-class-initialization-to-runtime=io.netty.handler.codec.http.HttpObjectEncoder,io.netty.handler.codec.http.websocketx.WebSocket00FrameEncoder,io.netty.handler.ssl.util.ThreadLocalInsecureRandom,com.sun.jndi.dns.DnsClient
RUN cd / && rm -rf target && rm -rf users-service-micro.jar
ENTRYPOINT ["./users-service-micro"]