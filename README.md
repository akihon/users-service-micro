# Users service micro

Micronaut framework micro service interacting with mongodb datastore to pull user information from mongo and return it 
to the originating request channel. 


These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

git clone ssh or https

What things you need to install the software and how to install them

```
Git 
Java 8+ 
GraalVM (latest) 
Maven 
Local mongo distribution
Docker (Windows use docker for windows latest) 
```

A step by step series of examples that tell you how to get a development env running

```
mvn clean install -DskipTests=true 
```

Your IDE will enable you to run the project as per the usual way, it's a sinple java project. 

```
curl http://localhost:8080/users/

[{"username":"Test1","firstName":"Test1","lastName":"Test1"},
{"username":"Test1","firstName":"Test1","lastName":"Test1"},
{"username":"Test1","firstName":"Test1","lastName":"Test1"},
{"username":"Test1","firstName":"Test1","lastName":"Test1"},
{"username":"test","firstName":"Test","lastName":"Test"},
{"username":"test","firstName":"Test","lastName":"Test"}]
```

* Note this is a standards java project which uses the micronaut framework, this does not change the way you would develop 
code, this doe snot run locally as a native image. It does not impact to any greater degree how you would normally develop your code 
the only time that you would see the code run as a native image would be if you were to deploy the code through the docker files 
either using the Dockerfile or to get the complete micro build using the DockerfileAllInOne this will produce an image of about 
59.6MB starting up in about 462ms. 


* [Micronaut](https://docs.micronaut.io/latest/guide/index.html) - The framework used

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

* Micronaut, loving what you guys are doing! 